<?php

$GLOBALS['TL_DCA']['tl_module']['palettes']['copyline'] =
    '{title_legend},name,type;'
    . '{copyline_legend},copylineText,copylineYearPosition;'
    . '{template_legend:hide},customTpl;'
    . '{protected_legend:hide},protected;'
    . '{expert_legend:hide},guests,cssID'
;

$GLOBALS['TL_DCA']['tl_module']['fields']['copylineText'] = array
(
  'exclude'                 => true,
  'search'                  => true,
  'inputType'               => 'text',
  'eval'                    => array('mandatory'=>true, 'allowHtml'=>true, 'tl_class'=>'w50'),
  'sql'                     => "mediumtext NULL"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['copylineYearPosition'] = array
(
  'exclude'                 => true,
  'inputType'               => 'radioTable',
  'options'                 => array('left', 'right'),
  'eval'                    => array('cols'=>2, 'tl_class'=>'w50'),
  'reference'               => &$GLOBALS['TL_LANG']['MSC'],
  'sql'                     => "varchar(32) NOT NULL default 'left'"
);
